<?php

use App\Models\Movie;
use App\Repository\ActorRepository;
use App\Repository\MovieRepository;

include_once __DIR__ . '/vendor/autoload.php';

$movieRepository = new MovieRepository();
$actorRepository = new ActorRepository();

$movie = $movieRepository->findAllMoviesWithActorToModel();
dump($movie);
