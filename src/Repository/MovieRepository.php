<?php

namespace App\Repository;

use App\Models\Actor;
use App\Models\Movie;
use App\Service\PDOService;
use DateTime;
use Exception;
use PDO;

class MovieRepository
{
    private PDOService $PDOService;
    private string $queryAll = 'SELECT * FROM movie';

    public function __construct()
    {
        $this->PDOService = new PDOService();
    }


    public function findAll():array
    {
        return $this->PDOService->getPDO()->query($this->queryAll)->fetchAll();
    }

    public function findFirstMovieToModel():Movie
    {
        return $this->PDOService->getPDO()->query($this->queryAll)->fetchObject(Movie::class);
    }

    public function findAllMoviesToModel():array
    {
        return $this->PDOService->getPDO()->query($this->queryAll)->fetchAll(PDO::FETCH_CLASS, Movie::class);
    }

    public function findAllMoviesWithActorToModel():array
    {
        $movies = $this->findAllMoviesToModel();
        $actorRepository = new ActorRepository();

        foreach ($movies as $movie){
            /** @var Movie $movie */
            $actors = $actorRepository->findAllActorsByMovie($movie->getId());
            dump($actors);
            foreach ($actors as $actor){
                $movie->addActor($actor);
            }
        }
        return $movies;
    }

//    public function findById(int $id): bool|Object
//    {
//        $query = $this->PDOService->getPDO()->prepare('SELECT id, title, release_date AS releaseDate FROM movie WHERE id = :id');
//        $query->bindParam(':id',$id);
//        $query->execute();
//
//        return $query->fetchObject();
//    }

    public function findById(int $id):Movie|bool
    {
        $query = $this->PDOService->getPDO()->prepare('SELECT DISTINCT id, title, release_date AS releaseDate FROM movie WHERE id = :id');
        $query->bindValue(':id',$id);
        $query->execute();

        return $query->fetchObject(Movie::class);
    }

    public function findByTitle(string $title):array
    {
        $query = $this->PDOService->getPDO()->prepare('SELECT * FROM movie WHERE title LIKE :title');
        $title = '%'.$title.'%';
        $query->bindParam(':title',$title);
        $query->execute();

        return $query->fetchAll(PDO::FETCH_CLASS,Movie::class);
    }

    /**
     * @throws Exception
     */
    public function insertMovie(Movie $movie): Movie
    {
        $query = $this->PDOService->getPDO()->prepare('INSERT INTO movie VALUE (null, :title, :release_date)');
        $title = $movie->getTitle();
        $releaseDate = $movie->getReleaseDate()->format('Y-m-d');
        $query->bindParam(':title',$title);
        $query->bindParam(':release_date',$releaseDate);
        $query->execute();

        return $movie;
    }

    public function addActorsToMovie(Movie $movie): Movie
    {
        $actors = $movie->getActors();
        foreach ($actors as $actor) {
            $query = $this->PDOService->getPDO()->prepare('INSERT INTO movie_actor VALUES (null,:id_movie,:id_actor)');

            $idMovie = $movie->getId();

            /** @var Actor $actor */
            $idActor = $actor->getId();

            $query->bindParam(':id_movie', $idMovie);
            $query->bindParam(':id_actor', $idActor);

            $query->execute();
        }
        return $movie;
    }

    public function deleteMovie(Movie $movie): Movie
    {
        $query = $this->PDOService->getPDO()->prepare('DELETE FROM movie WHERE id = :id');
        $id = $movie->getId();
        $query->bindParam(':id', $id);
        $query->execute();

        return $movie;
    }

    /**
     * @throws Exception
     */
    public function updateMovie(Movie $movie): Movie
    {
        $query = $this->PDOService->getPDO()->prepare('UPDATE movie set title = :title, release_date = :release_date WHERE id = :id');

        $id = $movie->getId();
        $title = $movie->getTitle();
        $date = $movie->getReleaseDate();

        $releaseDate = $date->format('Y-m-d');

        $query->bindParam(':id', $id);
        $query->bindParam(':title', $title);
        $query->bindParam(':release_date', $releaseDate);

        $query->execute();

        return $movie;
    }

    /**
     * @throws Exception
     */
    public function convertDataToObject(Object $dataBaseObject) : Movie
    {
        $movie = new Movie();
        $movie->setId($dataBaseObject->id);
        $movie->setTitle($dataBaseObject->title);
        $movie->setReleaseDate(New DateTime($dataBaseObject->releaseDate));

        return $movie;
    }
}