<?php

namespace App\Repository;

use App\Models\Actor;
use App\Service\PDOService;
use PDO;

class ActorRepository
{
    private PDOService $PDOService;
    private string $queryAll = 'SELECT * FROM actor';

    public function __construct()
    {
        $this->PDOService = new PDOService();
    }

    public function findAll():array
    {
        return $this->PDOService->getPDO()->query($this->queryAll)->fetchAll(PDO::FETCH_CLASS, Actor::class);
    }

    public function findAllActorsByMovie(int $id):array
    {
        $query = $this->PDOService->getPDO()
            ->prepare('
                SELECT ma.id, a.id, a.first_name AS firstName, a.last_name AS lastName
                FROM  movie_actor AS ma, actor AS a 
                WHERE ma.id_movie = :id
                AND a.id = ma.id_actor
                GROUP BY ma.id
                ');
        $query->bindParam(':id', $id);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_CLASS, Actor::class);
    }

    public function findById(int $id):Actor|bool
    {
        $query = $this->PDOService->getPDO()->prepare('SELECT id, first_name AS firstName, last_name AS lastName FROM actor WHERE id = :id');
        $query->bindParam(':id',$id);
        $query->execute();

        return $query->fetchObject(Actor::class);
    }

    public function insertActor(Actor $actor): Actor
    {
        $query = $this->PDOService->getPDO()->prepare('INSERT INTO actor VALUE (null,:first_name,:last_name)');
        $firstName = $actor->getFirstName();
        $lastName = $actor->getLastName();
        $query->bindParam(':first_name', $firstName);
        $query->bindParam(':last_name', $lastName);
        $query->execute();
        return $actor;
    }

    public function deleteActor(Actor $actor): void
    {
        $query = $this->PDOService->getPDO()->prepare('DELETE FROM actor WHERE id = :id');
        $id = $actor->getId();
        $query->bindParam(':id', $id);
        $query->execute();
    }

    public function updateActor(Actor $actor): Actor
    {
        $id = $actor->getId();
        $bddActor = $this->findById($id);
        if($bddActor !== $actor){
            $query = $this->PDOService->getPDO()->prepare('UPDATE actor SET first_name=:first_name, last_name=:last_name WHERE id=:id');
            $firstName = $actor->getFirstName();
            $lastName = $actor->getLastName();
            $query->bindParam(':id', $id);
            $query->bindParam(':first_name', $firstName);
            $query->bindParam(':last_name', $lastName);
            $query->execute();
        }
        return $actor;
    }
}